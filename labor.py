from download import download_data
from pathlib import Path
from workforce import workforce_calculation
from average_working_hours import working_hour

#15:00 22.03.22
storage_root = Path(".").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"

'''
URL of tables
'''

src_url = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/EMP_2EMP_SEX_ECO_NB_A.csv.gz"
src_csv = Path("EMP_2EMP_SEX_ECO_NB_A.csv")

src_url2 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/HOW_TEMP_SEX_ECO_NB_A.csv.gz"
src_csv2 = Path("HOW_TEMP_SEX_ECO_NB_A.csv")

src_url3 = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/lfsa_ewhuna.tsv.gz"
src_csv3 = Path("lfsa_ewhuna.tsv")


'''
Download compressed files, save them in "download" folder
uncompressed files and save them in "data" folder
'''
download_data(src_url,src_csv,src_url2,src_csv2,src_url3,src_csv3)


'''
Calculation of the workforce
'''

workforce = workforce_calculation(data_path,src_csv,src_csv2)
workforce.to_csv('workforce.csv')
workforce_old = workforce.copy()
'''
Calculation of the average number of working hours
'''


average_hour = working_hour(workforce,src_csv2,data_path,src_csv3) #15:50 le 25 mars
average_hour.to_csv('average_working_hour.csv')


