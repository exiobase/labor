import pandas as pd
from regression_ILO_region_with_minimum import regression
import country_converter as coco
from clean_workforce import clean
from clean_hour_list import clean_hour
from clean_hour_eurostat import reshape_eurostat
from isic3_to_isic4 import correspondance_isic
from substitute import substitute_isic_a
from combine_isic_3_4 import combine
from aggregate_isic import aggregate
from average_hour import average_working_hour
def working_hour(workforce,src_csv2,data_path,src_csv3):
        

    workforce,workforce2 = clean (workforce,coco)
   
    cc_all = coco.CountryConverter(include_obsolete=True)
    
    hour_list = pd.read_csv(data_path/src_csv2, encoding="utf-8-sig") 
    hour_list,hour_list_without_zero = clean_hour(hour_list)
                                
    hour_eurostat = pd.read_csv(data_path/src_csv3,  sep='\t|,')
    hour_eurostat_reshape = reshape_eurostat(hour_eurostat,cc_all)

                    
    hour_eurostat_reshape_pivot = hour_eurostat_reshape.pivot(index=['ref_area','sex','classif1'],columns='time')['obs_value']
    hour_eurostat_reshape_pivot_interpolate = hour_eurostat_reshape_pivot.interpolate(method='linear',axis=1,limit_area='inside')
    
    hour_eurostat_reshape_pivot_extrapolate = regression(hour_eurostat_reshape_pivot_interpolate,1992,2008)
    
    hour_eurostat_reshape_pivot_extrapolate.to_csv('hour_eurostat.csv')
    
    isic3=hour_list_without_zero[hour_list_without_zero['classif1'].str.contains('ISIC3',regex=True)].copy()
    isic4=hour_list_without_zero[hour_list_without_zero['classif1'].str.contains('ISIC4',regex=True)].copy()
    
    isic4_from_isic3_data = correspondance_isic(isic3)
    isic4_from_isic3_data.to_csv('isic4_from_isic3_data.csv') 
    
    '''
    Substituer ISIC A par la valeur de Eurostat pour 1995 a 2008
    '''
    isic4_from_isic3_data = substitute_isic_a(hour_eurostat_reshape_pivot_extrapolate,isic4_from_isic3_data)
   
                           
    isic4_from_isic3_data['obs_value'] = pd.to_numeric(isic4_from_isic3_data['obs_value'])
    isic4_from_isic3_data['time']=isic4_from_isic3_data['time'].astype(int)
    
    isic4_from_isic3_data_pivot = pd.pivot_table(isic4_from_isic3_data, values="obs_value", index=["ref_area", "sex","classif1"], columns=["time"])
    isic4_from_isic3_data_pivot=isic4_from_isic3_data_pivot.reset_index()
    isic4_from_isic3_data_pivot.to_csv("isic4_from_isic3_data_pivot.csv")
    
    
    new_table_150222=pd.DataFrame(data=None,columns=['ref_area','sex','classif1','time','obs_value','source'])
    new_table_150222_columns = new_table_150222.columns
    
    
    '''
    In order to get a new set of data, we choose to keep
    the data from ISIC4 from 2009
    the data from ISIC3 transformed to ISIC4 from 2009
    the data from ISIC3 before 2009
    '''
    
    new_table_150222 = combine(hour_list,isic4_from_isic3_data,new_table_150222,new_table_150222_columns,isic4)

    new_table_150222.to_csv('new_table_150222.csv')
    
    new_table_150222_pivot = pd.pivot_table(new_table_150222, values="obs_value", index=["ref_area", "sex","classif1"], columns=["time"])
    new_table_150222_pivot=new_table_150222_pivot.reset_index()
    
    
    new_table_150222 = aggregate(new_table_150222,new_table_150222_columns)

    
    new_table_150222.to_csv('new_table_150222_aggregate_ISIC.csv') 
    
    new_table_150222_pivot = new_table_150222.pivot(index=['ref_area','sex','classif1'],columns='time')['obs_value']
    new_table_150222_pivot.to_csv("new_table_150222_pivot_aggregate_ISIC.csv")  
    
    new_table_150222_pivot_interpolate = new_table_150222_pivot.interpolate(method='linear',axis=1,limit_area='inside')
    new_table_150222_pivot_interpolate=new_table_150222_pivot_interpolate.round(2)
    new_table_150222_pivot_interpolate.to_csv("new_table_150222_aggregate_ISIC_pivot_interpolate.csv")
    new_table_150222_pivot_interpolate_old=new_table_150222_pivot_interpolate.copy()
    
    
    
    new_table_150222_pivot_extrapolate = regression(new_table_150222_pivot_interpolate,1995,2020)
    new_table_150222_pivot_extrapolate=new_table_150222_pivot_extrapolate.round(2)
    new_table_150222_pivot_extrapolate.to_csv("new_table_150222_pivot_extrapolate_regression3_5_10.csv")
    
    
    country_code_final = list(new_table_150222_pivot_extrapolate.index.get_level_values(0))
    
    new_table_150222_pivot_extrapolate.insert(0, 'EXIO3', cc_all.convert(names = country_code_final,src="ISO3", to='EXIO3'))
    
    final_table =pd.DataFrame(data=None,columns=['EXIO3','sex','classif1','time','obs_value'])
    
    final_table = average_working_hour(new_table_150222_pivot_extrapolate,cc_all,workforce2,final_table)
  
    final_table.to_csv("final_table.csv",index = False)
    final_table = final_table[final_table.time != 2020]
#    ax1 = plt.boxplot
#    for code in final_table.EXIO3.unique():
#        for sex in final_table.sex.unique():
#            final_table[(final_table['EXIO3']==code)&(final_table['sex']==sex)].boxplot(by = 'classif1',column=['obs_value'],grid = False,figsize=(20,10))
#            
#            #plt.savefig('line_plot.pdf')
#            plt.savefig("{code}_{sex}.png".format(code=code,sex=sex))
#    
#    #import seaborn as sns
#    final_table_1998_2020 = final_table.copy()
#    final_table_1998_2020 = final_table_1998_2020.drop(columns=['1995','1996','1997'])
#            
#    ax1 = plt
#    sns.lmplot('EXIO3', 'obs_value', data=final_table, hue='classif1', fit_reg=False,size=5*3)
#    plt.show()
#    plt.savefig("improved_technique.png")
  
    return final_table