import pandas as pd

def reshape_eurostat(hour_eurostat,cc_all):    
    
    hour_eurostat = hour_eurostat[hour_eurostat['age'].str.contains('Y15-64',regex=True)]
    hour_eurostat = hour_eurostat[hour_eurostat['worktime'].str.contains('TOTAL',regex=True)]
    hour_eurostat = hour_eurostat[hour_eurostat['sex'].str.contains('F|M',regex=True)]
    hour_eurostat = hour_eurostat[hour_eurostat['wstatus'].str.contains('EMP',regex=True)]
    hour_eurostat = hour_eurostat[hour_eurostat['nace_r1'].str.contains('A_B',regex=True)]
    hour_eurostat = hour_eurostat[~hour_eurostat['geo\\time'].str.contains('EA19|EU15|EU27_2020|EU28')]
    hour_eurostat.columns = hour_eurostat.columns.str.replace(' ', '')
    for i in range(0,len(hour_eurostat.columns)):
        for j in range(0,len(hour_eurostat.index)):
            if (':') in hour_eurostat.iloc[j][i]:
                hour_eurostat.iloc[j][i]=''
                
    for i in range(7,len(hour_eurostat.columns)):
        for j in range(0,len(hour_eurostat.index)):
            #print(i,j)
            if ('b') in hour_eurostat.iloc[j][i]:
                hour_eurostat.iloc[j][i]=hour_eurostat.iloc[j][i].replace('b','')
            if ('c') in hour_eurostat.iloc[j][i]:
                hour_eurostat.iloc[j][i]=hour_eurostat.iloc[j][i].replace('c','')
            if ('bu') in hour_eurostat.iloc[j][i]:
                hour_eurostat.iloc[j][i]=hour_eurostat.iloc[j][i].replace('bu','')
            if ('u') in hour_eurostat.iloc[j][i]:
                hour_eurostat.iloc[j][i]=hour_eurostat.iloc[j][i].replace('u','')            
    
    '''
    pb with Greece and United Kingdom
    Made modification by hand
    '''
    
    
    cc_all.convert(names = hour_eurostat['geo\\time'],src="EXIO3", to='ISO3')
    hour_eurostat.insert(7, 'ISO3', cc_all.convert(names = hour_eurostat['geo\\time'],src="EXIO3", to='ISO3'))
    hour_eurostat=hour_eurostat.drop(columns='ISO3')
    hour_eurostat.insert(7, 'ISO3', cc_all.convert(names = hour_eurostat['geo\\time'],src="ISO2", to='ISO3'))
    hour_eurostat.to_csv('hour_eurostat.csv')
    hour_eurostat.insert(8, 'EXIO3', cc_all.convert(names = hour_eurostat['geo\\time'],src="ISO2", to='EXIO3'))
    hour_eurostat.to_csv('hour_eurostat.csv')
    hour_eurostat.loc[(hour_eurostat['geo\\time']=='EL'),['EXIO3']]='GR'
    hour_eurostat.loc[(hour_eurostat['geo\\time']=='EL'),['ISO3']]='GRC'
    hour_eurostat.loc[(hour_eurostat['geo\\time']=='UK'),['ISO3']]='GBR'
    hour_eurostat.loc[(hour_eurostat['geo\\time']=='UK'),['EXIO3']]='GB'
    hour_eurostat.loc[(hour_eurostat['geo\\time']=='UK'),['geo\\time']]='GB'
    hour_eurostat.loc[(hour_eurostat['geo\\time']=='EL'),['geo\\time']]='GR'
    
    hour_eurostat[hour_eurostat.columns[9:26]] = hour_eurostat[hour_eurostat.columns[9:26]].apply(pd.to_numeric)
    hour_eurostat = pd.concat([hour_eurostat[hour_eurostat.columns[1:8]], hour_eurostat[hour_eurostat.columns[9:26][:: -1]]], axis=1)
    hour_eurostat[hour_eurostat.columns[9:26]] = hour_eurostat[hour_eurostat.columns[9:26]].interpolate(method='linear',axis=1,limit_area='inside')
    
    hour_eurostat.loc[hour_eurostat.sex == "F", "sex"] = "SEX_F"
    hour_eurostat.loc[hour_eurostat.sex == "M", "sex"] = "SEX_M"
    hour_eurostat= hour_eurostat.drop(columns=['wstatus','worktime','age','unit','geo\\time'])
    hour_eurostat = hour_eurostat.iloc[:, [1, 0] + list(range(2, hour_eurostat.shape[1]))]
    hour_eurostat.insert(2, 'classif1', 'ECO_ISIC4_A')
    hour_eurostat = hour_eurostat.rename(columns={"ISO3": "ref_area"})
    
    for a in range(1992,2009):
        hour_eurostat = hour_eurostat.rename(columns={str(a): int(a)})
    
    hour_eurostat_reshape = pd.DataFrame(data=None,columns=['ref_area','sex','classif1','time','obs_value'])
    for code in hour_eurostat.ref_area.unique():
        for year in range(1992,2009):
            for sex in hour_eurostat.sex.unique():
                for classif in hour_eurostat.classif1.unique():
                    if not hour_eurostat.loc[(hour_eurostat['ref_area']==code)&(hour_eurostat['sex']==sex)&(hour_eurostat['classif1']==classif),[year]].isnull().values.all(): 
                        value =  float(hour_eurostat.loc[(hour_eurostat['ref_area']==code)&(hour_eurostat['sex']==sex)&(hour_eurostat['classif1']==classif),[year]].to_string(index=False, header=False))
                        hour_eurostat_reshape=hour_eurostat_reshape.append(pd.Series([code,sex,'ECO_ISIC3_A',year,value], index=[i for i in hour_eurostat_reshape]),ignore_index=True)
            
                        break
    return hour_eurostat_reshape
